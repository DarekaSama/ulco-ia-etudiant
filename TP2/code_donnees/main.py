import data_arbres
import numpy as np

db = data_arbres.load_data("tp_donnees.csv")


def proba_empirique(dataPoint):
    donnee = {}
    for elem in dataPoint:
        if elem.y not in donnee:
            #print("elem.y not in donnee : ", elem.y, "elem.x :", elem.x)
            donnee[elem.y] = 1
        if elem.y in donnee:
            donnee[elem.y] += 1
    for key in donnee:
        donnee[key] = donnee[key] / len(dataPoint)
    return donnee

def question_inf(dataX, attribut, valeur):
    return dataX[attribut] < valeur


def split(dataPoint, attribut, valeur):
    listV = []
    listF = []
    for elem in dataPoint:
        if question_inf(elem.x, attribut, valeur):
            listV.append(elem)
        else:
            listF.append(elem)
    return listF, listV


def list_separ_attributs(dataPoint, attribut):
    question = []
    separation = []
    for donnee in dataPoint:
        question.append((donnee.x[attribut]))
    question = list(set(question))
    question.sort()
    for i in range(len(question) - 1):
        separation.append((question[i] + question[i + 1]) / 2)
    return [(attribut, s) for s in np.sort(separation)]

def liste_questions(dataPoint):
    listeQuestion = []
    for donnee in dataPoint[0].x:
        listeQuestion += list_separ_attributs(dataPoint, donnee)
    return listeQuestion

def entropie(dataPoint):
    dataPoint = proba_empirique(dataPoint)
    calcul = 0
    for key, value in dataPoint.items():
        print("Key : ", key, " Value : ", value)
        if value != 0:
            logVal = np.log(value)
        else:
            logVal = 0
        calcul = calcul + (value * logVal)
        print("Calcul : ", calcul)
    calcul = -calcul
    return calcul

aie = proba_empirique(db)
print("Proba_empirique(db) : ", aie)
bol = split(db, "Age", 1)
print(" split() : ", bol[1])
vorce = list_separ_attributs(db, "Age")
print(" list_separ_attributs() : ", vorce)
ask = liste_questions(db)
print(" liste_questions() : ", ask)
intro = entropie(db)
print(" Entropie() : ", intro)

#aiee = split(db, "Age", 12)
#aiee2 = split(db, "Age", 12)
#print("------------000--------------")
#print(aiee)
