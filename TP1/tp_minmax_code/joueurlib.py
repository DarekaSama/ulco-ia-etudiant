import interfacelib
import numpy as np


class Joueur:
    def __init__(self, partie, couleur, opts={}):
        self.couleur = couleur
        self.couleurval = interfacelib.couleur_to_couleurval(couleur)
        self.jeu = partie
        self.opts = opts

    def demande_coup(self):
        pass


class Humain(Joueur):

    def demande_coup(self):
        pass


class IA(Joueur):

    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts)
        self.temps_exe = 0
        self.nb_appels_jouer = 0


class Random(IA):

    def demande_coup(self):
        coup_possible = self.jeu.plateau.liste_coups_valides(self.couleurval)
        # print(coup_possible)
        # print(self.couleurval)
        print(self.jeu.partie_finie)
        print(self.jeu.plateau.tableau_cases)
        print(self.jeu.plateau.existe_coup_valide(self.couleurval))
        nb = np.random.randint(0, len(coup_possible))
        return coup_possible[nb]


class Minmax(IA):

    def demande_coup(self):
		plateau = self.jeu.plateau.copie()
		result = self.evaluation(plateau)

        pass  # de même

    def minmax(noeud, joueur, depth):
        if noeud == 0:
            return (noeud, None)
        if joueur.couleurval == 1:
            print(joueur.couleurval)
            for element in joueur.plateau.liste_coups_valides(joueur.couleurval):  #
                noeudO = self.minmax(element, 2)[0]
                if (lowervalue < noeud):
                    lowervalue = noeud
                    argmax = element
            return (noeudO, argmax)
        elif joueur.couleurval == -1:  # max min
            for element in joueur.plateau.liste_coups_valides(joueur.couleurval):
                noeudT = Minmax.minmax(element, 1)[0]
                if highvalue > noeudT:
                    highvalue = noeudT
                    argmin = element
            return highvalue, argmin

    def evaluation(self, plateau):  # noeud, joueur):
		b, n = 0, 0
        for x in range(plateau.taille):
            for y in range(plateau.taille):
                if plateau.tableau_cases[x][y] == 1:
                    n += 1
                if plateau.tableau_cases[x][y] == -1:
                    b += 1
        return b - n


# heuristique nb noir - nb blanc
class AlphaBeta(IA):

    def demande_coup(self):
        pass  # you know the drill
